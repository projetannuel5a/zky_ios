//
//  CustomCartCell.swift
//  ZKY
//
//  Created by Saman Mohammad on 06/07/2017.
//  Copyright © 2017 esgi. All rights reserved.
//

import UIKit

class CustomCartCell: UITableViewCell {

    @IBOutlet weak var cartImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var qtiteLabel: UILabel!
    
    @IBAction func qtiteStepper(sender: UIStepper) {
        qtiteLabel.text = Int(sender.value).description
    }
}
