//
//  SecondViewController.swift
//  ZKY
//
//  Created by Saman Mohammad on 07/07/2017.
//  Copyright © 2017 esgi. All rights reserved.
//

import UIKit

class ConnectionViewController: UIViewController {
    
    
    @IBOutlet weak var mdpText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Looks for taps
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ConnectionViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func Connexion(sender: AnyObject) {
        if(mdpText.text != "" && emailText.text != ""){
            // create the request
            let url: String = "http://zky-catalogue.herokuapp.com/account/authenticate"
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            request.HTTPMethod = "POST"
            //let postString = "login=\(emailText.text)&password=\(mdpText.text)"
            
            let session = NSURLSession.sharedSession()
            
            let task = session.dataTaskWithRequest(request, completionHandler: {data, reponse, error -> Void in
                
                do {
                    _ = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                    // success
                    self.userDefaults.setBool(true, forKey: "connected")
                    self.dismissViewControllerAnimated(true, completion: nil)
                } catch let error as NSError {
                    // failure
                    print("Fetch failed: \(error.localizedDescription)")
                }
            })
            task.resume()
        } else{
            // display msg
            let toastLabel = UILabel(frame: CGRectMake(self.view.frame.size.width/2 - 150, self.view.frame.size.height-100, 300, 35))
            toastLabel.backgroundColor = UIColor.blackColor()
            toastLabel.textColor = UIColor.whiteColor()
            toastLabel.textAlignment = NSTextAlignment.Center;
            self.view.addSubview(toastLabel)
            toastLabel.text = "Veuillez saisir toute les informations"
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 10;
            toastLabel.clipsToBounds  =  true
            UIView.animateWithDuration(4.0, delay: 0.1, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                
                toastLabel.alpha = 0.0
                
                }, completion: nil)
        }
    }
    
    @IBAction func CancelConnexion(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil);
    }
}