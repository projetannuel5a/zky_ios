//
//  MonStyleViewController.swift
//  ZKY
//
//  Created by Saman Mohammad on 01/05/2017.
//  Copyright © 2017 esgi. All rights reserved.
//

import UIKit

class MonCatalogueController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    
    @IBOutlet weak var collectionView: UICollectionView!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var text: String = "produit"
    var productSelected: Product!
    var realProducts: [Product] = []
   
    
    @IBOutlet weak var spinnerMyStyle: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.spinnerMyStyle.startAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if(realProducts<10){
//            return realProducts.count
//        }else{
            return 10
//        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: CustomCollectionViewCell = self.collectionView.dequeueReusableCellWithReuseIdentifier("cellMyStyle", forIndexPath: indexPath) as! CustomCollectionViewCell
        
        // Configure the cell
        cell.lblCol.text = "Produit X"
        self.spinnerMyStyle.stopAnimating()
        self.spinnerMyStyle.hidden = true
        
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("MyCategToProduct", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "MyCategToProduct"){
            let controller:DetailViewController = segue.destinationViewController as! DetailViewController
            controller.productSelected = (sender as? Product)
        }
    }
    
}