//
//  CatalogueCategorieController.swift
//  ZKY
//
//  Created by Saman Mohammad on 09/05/2017.
//  Copyright © 2017 esgi. All rights reserved.
//

import UIKit

class CatalogueCategorieController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    
    @IBOutlet weak var collectionView: UICollectionView!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    var text: String = "produit"
    var productSelected: Product!
    var realProducts: [Product] = []
    var categories: [Product] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        // HTTP GET request to recover products
        let postUrl : NSURL = NSURL(string: "https://zky-catalogue.herokuapp.com/product")!
        let task = NSURLSession.sharedSession().dataTaskWithURL(postUrl){
            (data, response, error) -> Void in
            if(NetworkUtil.isConnectedToNetwork()){
                self.userDefaults.setObject(data, forKey: "json")
                self.userDefaults.synchronize()
                //self.filterJson(data!)
            }else {
                let savedData = self.userDefaults.objectForKey("json") as! NSData
                self.filterJson(savedData)
            }
        }
        task.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return realProducts.count
        return 4
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: CustomCollectionViewCell = self.collectionView.dequeueReusableCellWithReuseIdentifier("cellMyStyle", forIndexPath: indexPath) as! CustomCollectionViewCell
        
        //let product: Product = self.realProducts[indexPath.row]
        //cell.lblCol.text = "\(product.family)"
        cell.lblCol.text = "categorie X"
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("MyCateg", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "MyCateg"){
            let controller:MonCatalogueController = segue.destinationViewController as! MonCatalogueController
            controller.productSelected = (sender as? Product)
        }
    }
    
    // add categorie elements
    func filterProduct(){
        for item in categories as [Product]{
                self.realProducts.append(item)
         }
    }
    
    // json parsing
    func filterJson(data: NSData )-> Void {
        var json:AnyObject? = nil
        
        do{
            json = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
        }
        catch{
            print("Fail")
        }
        categories = []
        for item in json as! [[String: AnyObject]]{
            let id: String = String(item["id"])
            let name: String = String(item["name"])
            let family: String = String(item["family"])
            
            let product: Product = Product(id: id,
                                     name: name,
                                     family: family
            )
            self.categories.append(product)
        }
        self.filterProduct()
        print(self.realProducts)
        dispatch_async(dispatch_get_main_queue(), {self.collectionView.reloadData()})
    }
}