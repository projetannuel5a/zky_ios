//
//  WishlistController.swift
//  ZKY
//
//  Created by Saman Mohammad on 07/07/2017.
//  Copyright © 2017 esgi. All rights reserved.
//

import UIKit

class WishlistController: UIViewController,UITableViewDelegate ,UITableViewDataSource {
    
    
    @IBOutlet weak var wishlistTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.wishlistTableView.dataSource = self
        self.wishlistTableView.delegate = self
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: CustomWishlistCell = self.wishlistTableView.dequeueReusableCellWithIdentifier("CustomWishlistCell", forIndexPath: indexPath) as! CustomWishlistCell
        
        // Configure the cell
        cell.dateLbl.text = "01/03/2017"
        cell.titleLbl.text = "Commande 1"
        
        return cell
    }
}