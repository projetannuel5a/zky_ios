//
//  ViewController.swift
//  ZKY
//
//  Created by Etudiant on 01/05/2017.
//  Copyright © 2017 esgi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDefaults.setBool(false, forKey: "connected")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

