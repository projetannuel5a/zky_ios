//
//  CustomWishlistCell.swift
//  ZKY
//
//  Created by Saman Mohammad on 07/07/2017.
//  Copyright © 2017 esgi. All rights reserved.
//

import UIKit

class CustomWishlistCell: UITableViewCell {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
}
