//
//  DetailViewController.swift
//  ZKY
//
//  Created by Saman Mohammad on 01/05/2017.
//  Copyright © 2017 esgi. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var txtDetail: UITextView!
    @IBOutlet weak var imgDetail: UIImageView!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblUnity: UILabel!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    var productSelected: Product?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.lblDetail.text = self.productSelected?.title
        //self.txtDetail.text = self.productSelected?.about
        //print(self.productSelected?.tags)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func stepper(sender: UIStepper) {
        lblUnity.text = Int(sender.value).description
    }
    
    @IBAction func addToCartBtn(sender: AnyObject) {
        if(self.userDefaults.boolForKey("connected") == false) {
            let controller = storyboard?.instantiateViewControllerWithIdentifier("connexion") as! ConnectionViewController
            presentViewController(controller, animated: true, completion: nil)
        } else {
            //display msg
            let toastLabel = UILabel(frame: CGRectMake(self.view.frame.size.width/2 - 150, self.view.frame.size.height-100, 300, 35))
            toastLabel.backgroundColor = UIColor.blackColor()
            toastLabel.textColor = UIColor.whiteColor()
            toastLabel.textAlignment = NSTextAlignment.Center;
            self.view.addSubview(toastLabel)
            toastLabel.text = "Le produit a bien été ajouté au panier"
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 10;
            toastLabel.clipsToBounds  =  true
            UIView.animateWithDuration(4.0, delay: 0.1, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                
                toastLabel.alpha = 0.0
                
                }, completion: nil)

        }
        
        //print("ajout au panier")
        // Get the current product id
//        let productId = self.productSelected?.id
//        
//        if (productId != nil) {
            // Add the product to the cart
            //API.addElementToCart(...)
            
            // display msg to the user SUCCESS
//            let alert = UIAlertController(title: "Ajout au panier", message: "Votre produit a bien été ajouté", preferredStyle: UIAlertControllerStyle.Alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
//            self.presentViewController(alert, animated: true, completion: nil)
//        }
    }

}
