//
//  CustomCollectionViewLabelCell.swift
//  ZKY
//
//  Created by Saman Mohammad on 01/05/2017.
//  Copyright © 2017 esgi. All rights reserved.
//

import UIKit

class CustomCollectionViewLabelCell: UICollectionViewCell {

    @IBOutlet weak var labelTag: UILabel!
}