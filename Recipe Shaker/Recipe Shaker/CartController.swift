//
//  CartController.swift
//  ZKY
//
//  Created by Saman Mohammad on 03/07/2017.
//  Copyright © 2017 esgi. All rights reserved.
//

import UIKit

class CartController: UIViewController,UITableViewDelegate ,UITableViewDataSource {
    
    
    @IBOutlet weak var cartTableView: UITableView!
    let userDefaults = NSUserDefaults.standardUserDefaults()

    
    override func viewDidLoad() {
        
        //Force the user to connect
        if(userDefaults.boolForKey("connected") == false) {
            let controller = storyboard?.instantiateViewControllerWithIdentifier("connexion") as! ConnectionViewController
            presentViewController(controller, animated: true, completion: nil)
        } else {
            super.viewDidLoad()
            self.cartTableView.dataSource = self
            self.cartTableView.delegate = self
        }

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: CustomCartCell = self.cartTableView.dequeueReusableCellWithIdentifier("CustomCartCell", forIndexPath: indexPath) as! CustomCartCell
        
        
        // Configure the cell
        cell.cartImage.image = UIImage(named: "08-102")
        cell.titleLabel.text = "title product"
        cell.priceLabel.text = "0 €"
        cell.qtiteLabel.text = "1"
        
        return cell
    }
    
    // need this to allow deletion
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // handle delete
        }
    }
}