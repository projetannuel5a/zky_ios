//
//  Style.swift
//  ZKY
//
//  Created by Saman Mohammad on 01/05/2017.
//  Copyright © 2017 esgi. All rights reserved.
//

import Foundation

import UIKit

class Product: NSObject, NSCoding{
    var id: String
    var name: String
    var family: String
    
    override init(){
        self.id = "unknown"
        self.name = "unknown"
        self.family = "unknown"
    }
    
    required convenience init?(coder decoder: NSCoder) {
        guard let id = decoder.decodeObjectForKey("id") as? String,
            let name = decoder.decodeObjectForKey("name") as? String,
            let family = decoder.decodeObjectForKey("family") as? String
            else { return nil }
        
        self.init (
        id: id,
        name: name,
        family: family
        )
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.id, forKey: "id")
        coder.encodeObject(self.id, forKey: "name")
        coder.encodeObject(self.id, forKey: "family")
    }
    
    
    init(id: String,name: String,family: String) {
        
        self.id = id
        self.name = name
        self.family = family
    }
}